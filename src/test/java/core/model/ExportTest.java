package core.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;


public class ExportTest {

    @Test
    void createSimpleExport() {
        Export simpleExport = new Export();

        assertNotNull(simpleExport);
    }

    @Test
    void stupidExport() {
        Export stupidExport = null;

        assertNull(stupidExport);
    }
}
